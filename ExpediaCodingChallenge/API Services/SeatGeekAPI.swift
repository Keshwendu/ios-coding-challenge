//
//  SearchService.swift
//  ExpediaCodingChallenge
//
//  Created by Keshwendu Kant Suryabansi on 21/04/19.
//  Copyright © 2019 Keshwendu Kant Suryabansi. All rights reserved.
//

import Moya

public enum SeatGeekAPI {
    case searchEvents(query: String)
}

extension SeatGeekAPI: TargetType {
    
    public var baseURL: URL { return URL(string: API.baseAPIPath)! }
    
    public var path: String {
        switch self {
        case .searchEvents:
            return "2/events"
        }
    }
    
    public var method: Moya.Method {
        switch self {
        case .searchEvents:
            return .get
        }
    }
    
    public var task: Task {
        switch self {
        case .searchEvents(let q):
            return .requestParameters(parameters: ["client_id": API.clientID, "q": q], encoding: URLEncoding.queryString)
        }
    }
    
    public var headers: [String: String]? {
        return nil
    }
    
    public var validationType: ValidationType {
        switch self {
        case .searchEvents:
            return .successCodes
        }
        
    }
    
    public var sampleData: Data {
        switch self {
        case .searchEvents:
            return "[{}]".data(using: String.Encoding.utf8)!
        }
    }
}


