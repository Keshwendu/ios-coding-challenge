//
//  SearchListVC.swift
//  ExpediaCodingChallenge
//
//  Created by Keshwendu Kant Suryabansi on 21/04/19.
//  Copyright © 2019 Keshwendu Kant Suryabansi. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class SearchListVC: UIViewController {
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var listTableView: UITableView!

    private let disposeBag = DisposeBag()
    var viewModel = SearchListViewModel()
    
    var searchText: Observable<String> {
        return self.searchBar.rx.text.orEmpty.throttle(0.3, scheduler: MainScheduler.instance).distinctUntilChanged()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupBinding()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        listTableView.reloadRows(at: listTableView.indexPathsForVisibleRows ?? [], with: .fade)
    }
    
    private func setupBinding() {
        listTableView.register(UINib(nibName: "SearchItemCell", bundle: nil), forCellReuseIdentifier: String(describing: SearchItemCell.self))
        
        viewModel.loading
            .bind(to: self.rx.isAnimating).disposed(by: disposeBag)
        
        viewModel
            .events
            .observeOn(MainScheduler.instance)
            .bind(to: listTableView.rx.items(cellIdentifier: "SearchItemCell", cellType: SearchItemCell.self)) {  (row,ev,cell) in
                cell.event = ev
            }.disposed(by: disposeBag)

        listTableView.rx.willDisplayCell
            .subscribe(onNext: ({ (cell,indexPath) in
                cell.alpha = 0
                let transform = CATransform3DTranslate(CATransform3DIdentity, -250, 0, 0)
                cell.layer.transform = transform
                UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: {
                    cell.alpha = 1
                    cell.layer.transform = CATransform3DIdentity
                }, completion: nil)
            })).disposed(by: disposeBag)
        
        listTableView.rx.modelSelected(Event.self).bind {[unowned self] eventModel in
            self.viewModel.selectedEvent.onNext(eventModel)
        }.disposed(by: disposeBag)
        
        searchBar.rx.cancelButtonClicked.subscribe(onNext: {[unowned self] () in
            self.searchBar.resignFirstResponder()
        }).disposed(by: disposeBag)
        
        viewModel.requestData(query: self.searchText)
    }
}
