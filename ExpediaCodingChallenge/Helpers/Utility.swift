//
//  Utility.swift
//  ExpediaCodingChallenge
//
//  Created by Keshwendu Kant Suryabansi on 23/04/19.
//  Copyright © 2019 Keshwendu Kant Suryabansi. All rights reserved.
//

import Foundation

struct Utility {
    static func dateString(_ str: String?) -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        guard let str = str else { return "" }
        let date = dateFormatterGet.date(from: str)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "E, d MMM yyyy HH:mm a"
        if let date = date {
            return dateFormatter.string(from: date)
        }
        return ""
    }
}
