//
//  LoadingViewExtensions.swift
//  ExpediaCodingChallenge
//
//  Created by Keshwendu Kant Suryabansi on 23/04/19.
//  Copyright © 2019 Keshwendu Kant Suryabansi. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

extension UIViewController: LoadingViewable {}

extension Reactive where Base: UIViewController {
    
    /// Bindable sink for `startAnimating()`, `stopAnimating()` methods.
    public var isAnimating: Binder<Bool> {
        return Binder(self.base, binding: { (vc, active) in
            if active {
                vc.startAnimating()
            } else {
                vc.stopAnimating()
            }
        })
    }
    
}
