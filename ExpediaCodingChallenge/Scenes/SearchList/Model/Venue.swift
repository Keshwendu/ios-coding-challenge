//
//  Venue.swift
//  ExpediaCodingChallenge
//
//  Created by Keshwendu Kant Suryabansi on 23/04/19.
//  Copyright © 2019 Keshwendu Kant Suryabansi. All rights reserved.
//

import Foundation

struct Venue: Codable {
    let extendedAddress, displayLocation, address, name: String?
    
    enum CodingKeys: String, CodingKey {
        case extendedAddress = "extended_address"
        case displayLocation = "display_location"
        case address, name
    }
}
