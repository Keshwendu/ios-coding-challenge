//
//  SearchListCoordinator.swift
//  ExpediaCodingChallenge
//
//  Created by Keshwendu Kant Suryabansi on 21/04/19.
//  Copyright © 2019 Keshwendu Kant Suryabansi. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

class SearchListCoordinator: BaseCoordinator<Void> {
    
    private weak var window: UIWindow?
    
    init(window: UIWindow) {
        self.window = window
    }
    
    override func start() -> Observable<Void> {
        let searchVC = SearchListVC(nib: R.nib.searchList)
        searchVC.viewModel = SearchListViewModel()
        let navigationVC = UINavigationController(rootViewController: searchVC)
        navigationVC.setNavigationBarHidden(true, animated: false)
        searchVC.viewModel.selectedEvent.observeOn(MainScheduler.instance).subscribe(onNext: { [weak self] eventModel in
            self?.displayEventDetailsScreen(navigationController: navigationVC, searchDetailsViewModeling: SearchDetailsViewModel(event: eventModel, db: DataProvider()))
        }).disposed(by: disposeBag)
        window?.rootViewController = navigationVC
        window?.makeKeyAndVisible()
        return .never()
    }
    
}

fileprivate extension SearchListCoordinator {
    
    func displayEventDetailsScreen(navigationController: UINavigationController, searchDetailsViewModeling: SearchDetailsViewModel) {
        let detailsVC = SearchDetailsVC(nib: R.nib.searchDetailsVC)
        detailsVC.viewModel = searchDetailsViewModeling
        navigationController.pushViewController(detailsVC, animated: true)
    }
    
}
