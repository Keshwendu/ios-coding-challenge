//
//  SearchDetailsViewModel.swift
//  ExpediaCodingChallenge
//
//  Created by Keshwendu Kant Suryabansi on 22/04/19.
//  Copyright © 2019 Keshwendu Kant Suryabansi. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa


class SearchDetailsViewModel {
    
    public enum searchError {
        case internetError(String)
        case serverMessage(String)
    }
    
    public let event: BehaviorSubject<Event> = BehaviorSubject(value: Event(identifier: 0, title: "", announceDate: nil, datetimeLocal: nil, visibleUntilUtc: nil, venue: nil, performers: nil))
    
    public let isFavorite: PublishSubject<Bool> = PublishSubject()
    
    private let disposable = DisposeBag()
    private let db: DataProvider
    
    init(event: Event, db: DataProvider) {
        self.db = db
        self.event.onNext(event)
        
        // Update favorite and unfavorite
        self.isFavorite.subscribe(onNext: { isFav in
            if !isFav {
                let favEvent = db.objects(Favorite.self)?.filter("eventId = \(event.identifier)").first
                if let favEvent = favEvent {
                    db.delete(favEvent)
                }
            } else {
                db.add(Favorite(event: event))
            }
        }).disposed(by: disposable)
    }
}
