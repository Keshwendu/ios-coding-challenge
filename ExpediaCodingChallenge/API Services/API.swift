//
//  API.swift
//  ExpediaCodingChallenge
//
//  Created by Keshwendu Kant Suryabansi on 21/04/19.
//  Copyright © 2019 Keshwendu Kant Suryabansi. All rights reserved.
//

import Foundation
import Moya
import RxSwift

class API {
    static let baseAPIPath = "https://api.seatgeek.com/"
    static let clientID = "MTYyMjUyOTJ8MTU1NTMzODU3My4wMg"
    
    static let provider = MoyaProvider<SeatGeekAPI>()
    
//    static let provider = MoyaProvider<SeatGeekAPI>(plugins: [NetworkLoggerPlugin(verbose: true)])
    
   static func searchEvents(query: String) -> Observable<SearchListResponse> {
        return API.provider.rx
            .request(.searchEvents(query: query)) 
            .map(SearchListResponse.self)
            .catchError({ (err) -> PrimitiveSequence<SingleTrait, SearchListResponse> in
                return .just(SearchListResponse(meta: Meta(perPage: 0, total: 0, took: 0, page: 0), events: []))
            })
            .asObservable()
    }
}
