//
//  SearchListResponse.swift
//  ExpediaCodingChallenge
//
//  Created by Keshwendu Kant Suryabansi on 23/04/19.
//  Copyright © 2019 Keshwendu Kant Suryabansi. All rights reserved.
//

import Foundation

struct SearchListResponse: Codable {
    let meta: Meta?
    let events: [Event]?
    
    enum CodingKeys: String, CodingKey {
        case meta, events
    }
}


// MARK: Convenience initializers

extension SearchListResponse {
    init?(data: Data) {
        guard let me = try? JSONDecoder().decode(SearchListResponse.self, from: data) else { return nil }
        self = me
    }
}
