//
//  Favorite.swift
//  ExpediaCodingChallenge
//
//  Created by Keshwendu Kant Suryabansi on 23/04/19.
//  Copyright © 2019 Keshwendu Kant Suryabansi. All rights reserved.
//

import Foundation
import RealmSwift

class Favorite: Object {
    @objc dynamic var eventId: Int64 = 0
    
    override static func primaryKey() -> String? { return "eventId" }
    
    convenience init(event: Event) {
        self.init()
        eventId = event.identifier
    }
}
