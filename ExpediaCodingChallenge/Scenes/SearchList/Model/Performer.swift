//
//  Performer.swift
//  ExpediaCodingChallenge
//
//  Created by Keshwendu Kant Suryabansi on 23/04/19.
//  Copyright © 2019 Keshwendu Kant Suryabansi. All rights reserved.
//

import Foundation

struct Performer: Codable {
    let image: String?
    let primary: Bool?
    
    enum CodingKeys: String, CodingKey {
        case image, primary
    }
}
