# Summary
Write a type ahead against the Seat Geek API. The type ahead should update a list of results as the search query changes. Results can be tapped to view them on a details screen. On the details screen the result can be favorited. On the type ahead screen, results matching the favorites list should show a visual indication that they are favorited.

## To summarize

- Write a type ahead against the Seat Geek API
- Make a detail screen so the user can drill down into a result
- The detail screen should allow the user to favorite/unfavorite the event
- Type ahead results should reflect the favorited state of each event
- Favorited results should be saved between launches of the app

# Requirements
- XCode 10.2

# Install
Open the terminal and go to the checked out directory and run below command:

`pod install`

