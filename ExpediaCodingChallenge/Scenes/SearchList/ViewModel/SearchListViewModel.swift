//
//  SearchListViewModel.swift
//  ExpediaCodingChallenge
//
//  Created by Keshwendu Kant Suryabansi on 21/04/19.
//  Copyright © 2019 Keshwendu Kant Suryabansi. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa


class SearchListViewModel {
    
    public enum searchError {
        case internetError(String)
        case serverMessage(String)
    }
    
    public let events : PublishSubject<[Event]> = PublishSubject()
    public let selectedEvent : PublishSubject<Event> = PublishSubject()
    public let loading: PublishSubject<Bool> = PublishSubject()
    public let error : PublishSubject<searchError> = PublishSubject()
    
    private let disposable = DisposeBag()
    
    public func requestData(query: Observable<String>) {
        query.flatMapLatest { (q) -> Observable<SearchListResponse> in
            self.loading.onNext(true)
            return API.searchEvents(query: q)
            }
            .map { $0.events ?? [] }
            .subscribe(onNext: {
                self.events.onNext($0)
                self.loading.onNext(false)
            }).disposed(by: disposable)
    }
}

