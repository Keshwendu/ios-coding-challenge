//
//  SearchItemCell.swift
//  ExpediaCodingChallenge
//
//  Created by Keshwendu Kant Suryabansi on 21/04/19.
//  Copyright © 2019 Keshwendu Kant Suryabansi. All rights reserved.
//

import UIKit
import Kingfisher

class SearchItemCell: UITableViewCell {

    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var favoriteView: UIImageView!
    
    public var event : Event! {
        didSet {
            self.title.text = event.title
            self.address.text = event.venue?.displayLocation ?? ""
            self.date.text = Utility.dateString(event.announceDate)
            if let img = event.image {
                let imgurl = URL(string: img)
                self.mainImageView.kf.setImage(with: imgurl)
            } else {
                self.mainImageView.image = UIImage(named: "no_image")
            }
            favoriteView.isHidden = !event.isFavorite
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        mainImageView.image = UIImage()
    }
    
}
