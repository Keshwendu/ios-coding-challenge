//
//  SearchDetailsVC.swift
//  ExpediaCodingChallenge
//
//  Created by Keshwendu Kant Suryabansi on 22/04/19.
//  Copyright © 2019 Keshwendu Kant Suryabansi. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Kingfisher

class SearchDetailsVC: UIViewController {

    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var addressLbl: UILabel!
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var favBtn: UIButton!
    
    private let disposeBag = DisposeBag()
    var viewModel: SearchDetailsViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateFavBtnStyle()
        setupBinding()
    }

    private func setupBinding() {
        viewModel.event.observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] eventModel in
            if let img = eventModel.image {
                self.mainImageView.kf.setImage(with: URL(string: img))
            } else {
                self.mainImageView.image = UIImage(named: "no_image")
            }
            self.dateLbl.text = Utility.dateString(eventModel.announceDate)
            self.addressLbl.text = eventModel.venue?.displayLocation ?? ""
            self.titleLbl.text = eventModel.title
            self.updateFavBtnStyle(isFavorite: eventModel.isFavorite)
        }).disposed(by: disposeBag)
        
        viewModel.isFavorite.observeOn(MainScheduler.instance).subscribe(onNext: { [unowned self] isFav in
            self.updateFavBtnStyle(isFavorite: isFav)
        }).disposed(by: disposeBag)
        
        favBtn.rx.tap.throttle(1.0, scheduler: MainScheduler.instance).subscribe ( onNext: { [unowned self] _ in
            self.viewModel.isFavorite.onNext(!self.favBtn.isSelected)
        }).disposed(by: disposeBag)
        
        backBtn.rx.tap.throttle(1.0, scheduler: MainScheduler.instance).subscribe ( onNext: { [unowned self] _ in
            self.navigationController?.popViewController(animated: true)
        }).disposed(by: disposeBag)
    }
    
    private func updateFavBtnStyle(isFavorite: Bool = false) {
        favBtn.isSelected = isFavorite
    }
    
}
