//
//  Event.swift
//  ExpediaCodingChallenge
//
//  Created by Keshwendu Kant Suryabansi on 21/04/19.
//  Copyright © 2019 Keshwendu Kant Suryabansi. All rights reserved.
//

import Foundation


struct Event: Codable {
    let identifier: Int64
    let title: String
    let announceDate: String?
    let datetimeLocal: String?
    let visibleUntilUtc: String?
    let venue: Venue?
    let performers: [Performer]?
    
    var image: String? {
        let performer = self.performers?.filter({ $0.primary ?? false }).first
        if let performer = performer, let img = performer.image {
            return img
        } else {
            return nil
        }
    }
    
    var isFavorite: Bool {
        return DataProvider().objects(Favorite.self)?.filter("eventId = \(self.identifier)").count ?? 0 > 0
    }
    
    enum CodingKeys: String, CodingKey {
        case identifier = "id"
        case title
        case announceDate = "announce_date"
        case datetimeLocal = "datetime_local"
        case visibleUntilUtc = "visible_until_utc"
        case venue, performers
    }
}
