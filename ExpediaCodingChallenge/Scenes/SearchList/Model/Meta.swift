//
//  Meta.swift
//  ExpediaCodingChallenge
//
//  Created by Keshwendu Kant Suryabansi on 23/04/19.
//  Copyright © 2019 Keshwendu Kant Suryabansi. All rights reserved.
//

import Foundation

struct Meta: Codable {
    let perPage, total, took, page: Int
    
    enum CodingKeys: String, CodingKey {
        case perPage = "per_page"
        case total, took, page
    }
}
